#ifndef ColoredTracers_H
#define ColoredTracers_H

#include "../base/d3d9/d3drender.h"
#include "../base/d3d9/imgui/imgui.h"
#include "../base/d3d9/imgui_expand.h"
#include "../base/d3d9/notifications.h"
#include "../base/d3d9/texture.h"
#include "loader/loader.h"

#include "../FileMan/ciniserialization.h"
#include "../base/d3d9/ImWrapper/ImWrapper.hpp"

class ColoredTracers : public SRDescent, public CIniSerialization {
public:
	explicit ColoredTracers();
	virtual ~ColoredTracers();

	void command( char * );

	void updateOldTracers();

private:
	CCallHook *_hook_render;
	CCallHook *_hook_render2;

	CCallHook *_hook_calc;
	CCallHook *_hook_addTrace;
	CCallHook *_hook_color;

	CCallHook *_hook_limit;

	ImMenu *_menu;

	int _activePlayerId;
	std::map<uint, uint> _colors;

	SERIALIZE_EXT( bool, trace__replaceOlder, false )
	SERIALIZE_EXT( bool, trace__longTracers, false )
	SERIALIZE_EXT( float, trace__radius, 0.01f )
	SERIALIZE_EXT( uint, trace__time, 300 )
	SERIALIZE_EXT( byte, trace__alpha, 70 )
	SERIALIZE_EXT( uint, trace__limit, 128 )

	SERIALIZE_EXT( std::string, chat__command, "trac" )

	RwV3D _trStart, _trEnd;

	CBulletTrace *_aTraces = (CBulletTrace *)0xC7C748;

	CBulletTrace *_bulletCollection;
	byte _activeBullet;

	void reallocateTracesTable( uint size );
	void restoreTracesTable();
	void setTracesTable();

	void hook_CBulletTraces__Render_start();
	void hook_CBulletTraces__Render_drawTrace();

	void hook_CBulletTraces__AddTrace( RwV3D *&start, RwV3D *&end );
	void hook_CBulletTrace__AddTrace( RwV3D *&start, RwV3D *&end, float &radius, uint &time, byte &alpha );
	void hook_CBulletTrace__AddTrace_color();
	void hook_CBulletTrace__AddTrace_more120();
};

#endif // ColoredTracers_H

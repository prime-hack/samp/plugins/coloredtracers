#include "loader.h"
#include "../../base/d3d9/proxydirectx.h"
#include "../ColoredTracers.h"

SRString PROJECT_NAME = "ColoredTracers";
eCursorType cursorType = eCursorType::System;
stGlobalHandles g_handle;
stGlobalClasses g_class;
stGlobalPVars g_vars;

static LPTOP_LEVEL_EXCEPTION_FILTER hOrigUEF;
static ColoredTracers *pColoredTracers = static_cast<ColoredTracers *>( nullptr );
static CCallHook *sampShowCursor;
static IDirect3DDevice9 *device;
static SRString PLUGIN_NAME;
static SRString PLUGIN_PATH;
static HMODULE PLUGIN_MODULE;

void InstallD3DHook() {
	static bool isDxHooked = false;
	if ( isDxHooked ) return;
	isDxHooked = true;
	device = new proxyIDirect3DDevice9( *reinterpret_cast<IDirect3DDevice9 **>( 0xC97C28 ) );
	*reinterpret_cast<IDirect3DDevice9 **>( 0xC97C28 ) = dynamic_cast<IDirect3DDevice9 *>( device );
	//	if (device == nullptr)
	//		g_class.DirectX = new proxyIDirect3DDevice9( *reinterpret_cast<IDirect3DDevice9 **>(
	//g_handle.dwSAMP
	//+ sampOffset::dxInfo() ) ); 	else g_class.DirectX = new proxyIDirect3DDevice9( device );
	//	*reinterpret_cast<IDirect3DDevice9 **>(g_handle.dwSAMP + sampOffset::dxInfo()) =
	// dynamic_cast<IDirect3DDevice9*>(g_class.DirectX); 	g_class.DirectX = new CDirectX(device);
	g_class.DirectX = (proxyIDirect3DDevice9 *)device;
}

LONG WINAPI unhandledExceptionFilter( struct _EXCEPTION_POINTERS * ) {
	/* The game can crash on the D3D destructor if another
	 * plugin installs the D3D hook after this plugin,
	 * and this plugin will be unloaded without unloading
	 * the other plugin.
	 */
	return TerminateProcess( GetCurrentProcess(), 0 );
}

static CALLHOOK ShowCursor() {
	if ( g_class.cursor->isLocalCursorHiden() ) return;

	if ( cursorType == eCursorType::System )
		SetCursor( LoadCursor( nullptr, IDC_ARROW ) );
	else if ( cursorType == eCursorType::ByImGui )
		SetCursor( LoadCursor( nullptr, nullptr ) );
}

CALLHOOK GameLoop() {
	g_class.cursor = SRCursor::Instance();
	g_handle.d3d9 = GetModuleHandleA( "d3d9.dll" );
	if ( g_handle.d3d9 == nullptr || g_handle.d3d9 == reinterpret_cast<HANDLE>( -1 ) ) return;
	InstallD3DHook();

	g_handle.samp = GetModuleHandleA( "samp.dll" );
	if ( g_handle.isSAMP() ) {

		g_class.samp = *reinterpret_cast<class SAMP **>( g_handle.dwSAMP + sampOffset::Info() );
		if ( g_class.samp == nullptr ) return;
	}

	g_vars.pluginName = PLUGIN_NAME;
	g_vars.pluginPath = PLUGIN_PATH;
	g_handle.plugin = PLUGIN_MODULE;

	static bool hooked = false;
	if ( hooked ) return;
	hooked = true;

	if ( g_handle.isSAMP() ) g_class.samp->misc()->toggleCursor( false ); // For reload cursor
	init_ImGui_events();
	g_class.events = SREvents::Instance();
	pColoredTracers = new ColoredTracers();
	if ( cursorType == eCursorType::ByImGui ) g_class.cursor->setCursor( nullptr );
	if ( g_handle.isSAMP() ) {
		sampShowCursor = new CCallHook(
			reinterpret_cast<void *>( g_handle.dwSAMP + sampOffset::funcToggleCursor() + 0x69 ), 8, 0,
			cp_before );
		sampShowCursor->enable( ShowCursor );
	}
#ifndef DEBUG
	hOrigUEF = SetUnhandledExceptionFilter( unhandledExceptionFilter );
#endif
}

BOOL APIENTRY DllMain( HMODULE hModule, DWORD dwReasonForCall, LPVOID ) {
	static CCallHook *gameloopHook;

	if ( dwReasonForCall == DLL_PROCESS_ATTACH ) {

		if ( sizeof( CPed ) != 1988 ) {
			MessageBox( "Incorrect CPed == " + std::to_string( sizeof( CPed ) ) );
			return FALSE;
		}
		if ( sizeof( CVehicle ) != 2584 ) {
			MessageBox( "Incorrect CVehicle == " + std::to_string( sizeof( CVehicle ) ) );
			return FALSE;
		}
		char name[256];
		GetModuleFileNameA( hModule, name, 256 );
		std::cmatch m;
		std::regex re( R"((.*\\)((.*).asi))", std::regex::icase );
		SRString fullPluginName;
		if ( std::regex_match( name, m, re ) ) {
			PLUGIN_PATH = m[1].str();
			fullPluginName = m[2].str();
			PLUGIN_NAME = m[3].str();
		}
#ifndef _ALLOWRENAME
		if ( GetModuleHandleA( ( PROJECT_NAME + ".asi" ).c_str() ) != hModule ) {
			if ( !fullPluginName.isEmpty() )
				MessageBox( "Incorrect file name.\n"
							"Please rename " +
							fullPluginName + " to " + PROJECT_NAME + ".asi" );
			return FALSE;
		}
#endif // _ALLOWRENAME

		gameloopHook = new CCallHook( reinterpret_cast<void *>( 0x00748DA3 ), 6 );
		gameloopHook->enable( GameLoop );
	} else if ( dwReasonForCall == DLL_THREAD_ATTACH ) {
		if ( g_class.events ) g_class.events->Inject();
	} else if ( dwReasonForCall == DLL_THREAD_DETACH ) {
		if ( g_class.events ) g_class.events->Deinject();
	} else if ( dwReasonForCall == DLL_PROCESS_DETACH ) {
		delete gameloopHook;
		delete sampShowCursor;
		delete pColoredTracers;
		pColoredTracers = nullptr;
		SF::DeleteInstance();
		delete g_class.DirectX;
		g_class.cursor->DeleteInstance();
		g_class.events->DeleteInstance();
		deinit_ImGui_events();
#ifndef DEBUG
		SetUnhandledExceptionFilter( hOrigUEF );
#endif
	}

	return TRUE;
}

int MessageBox( SRString text, SRString title, UINT type ) {
	return MessageBoxA( g_vars.hwnd, text.c_str(), title.c_str(), type );
}

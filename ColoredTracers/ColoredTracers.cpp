#include "ColoredTracers.h"
#include "../base/CGame/methods.h"

ColoredTracers::ColoredTracers() : SRDescent(), CIniSerialization( PROJECT_NAME + ".ini" ) {
	_hook_render = new CCallHook( (void *)0x00723CA9, 6 );
	_hook_render->enable( this, &ColoredTracers::hook_CBulletTraces__Render_start );

	_hook_render2 = new CCallHook( (void *)0x00723D9D, 7 );
	_hook_render2->enable( this, &ColoredTracers::hook_CBulletTraces__Render_drawTrace );

	_hook_calc = new CCallHook( (void *)0x00726AF0, 6, 2 );
	_hook_calc->enable( this, &ColoredTracers::hook_CBulletTraces__AddTrace );

	_hook_addTrace = new CCallHook( (void *)0x00723750, 5, 5 );
	_hook_addTrace->enable( this, &ColoredTracers::hook_CBulletTrace__AddTrace );

	_hook_color = new CCallHook( (void *)0x007238CA, 5, 0, cp_before );
	_hook_color->enable( this, &ColoredTracers::hook_CBulletTrace__AddTrace_color );

	memsafe::copy( (void *)0x72388F, {0x90, 0x39, 0xCE, 0x8B, 0xCE, 0x7D, 0x61} );
	_hook_limit = new CCallHook( (void *)0x72388F );
	_hook_limit->enable( this, &ColoredTracers::hook_CBulletTrace__AddTrace_more120 );

	// Уничтожение калькулятора времени жизни
	memsafe::set( (void *)0x7237B6, 0x90, 3 );
	memsafe::set( (void *)0x7237BC, 0x90, 73 );

	_menu = new ImMenu( PROJECT_NAME );
	_menu->flags |=
		ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse;
	_menu->onHide += []() {
		g_class.cursor->hideCursor();
		g_class.samp->chat()->lockChatOnCharT( false );
	};

	auto itCommand = new ImInputText( "command", chat__command(), 32 );
	itCommand->onTextChanged += [this, itCommand]( std::string text ) {
		if ( text.empty() ) {
			_menu->closable = false;
			return;
		} else if ( !_menu->closable )
			_menu->closable = true;
		g_class.samp->input()->changeCommand( chat__command(), text );
		chat__command() = text;
	};

	auto cbReplaceOlder = new ImCheckBox( "replace older    ", trace__replaceOlder() );
	cbReplaceOlder->onToggle += [this]( bool checked ) { trace__replaceOlder() = checked; };

	auto cbLongTracers = new ImCheckBox( "long tracers", trace__longTracers() );
	cbLongTracers->onToggle += [this]( bool checked ) { trace__longTracers() = checked; };

	auto dfRadius = new ImDragFloat( "radius", trace__radius() );
	dfRadius->min = 0.001;
	dfRadius->max = 0.05;
	dfRadius->speed = 0.001;
	dfRadius->onFloatChanged += [this]( float value ) {
		trace__radius() = value;
		updateOldTracers();
	};

	auto iiTime = new ImInputInt( "time", trace__time() );
	iiTime->onIntChanged += [this, iiTime]( int value ) {
		if ( value < 0 ) {
			value = 0;
			iiTime->setValue( 0 );
		}
		trace__time() = value;
	};

	auto iiAlpha = new ImInputInt( "transparent", trace__alpha() );
	iiAlpha->onIntChanged += [this, iiAlpha]( int value ) {
		if ( value < 0 ) {
			value = 0;
			iiAlpha->setValue( 0 );
		} else if ( value > 255 ) {
			value = 255;
			iiAlpha->setValue( 255 );
		}
		trace__alpha() = value;
	};

	auto iiLimit = new ImInputInt( "tracers limit", trace__limit() );
	iiLimit->step = 8;
	iiLimit->onIntChanged += [this, iiLimit]( int value ) {
		if ( value < 16 ) {
			iiLimit->setValue( 16 );
			return;
		}
		if ( value % 8 != 0 ) {
			iiLimit->setValue( round( (float)value / 8.0f ) * 8.0f );
			return;
		}
		reallocateTracesTable( value );
	};

	_menu->chields.push_back( itCommand );
	_menu->chields.push_back( new ImSeparator() );
	_menu->chields.push_back( cbReplaceOlder );
	_menu->chields.push_back( new ImSameLine() );
	_menu->chields.push_back( cbLongTracers );
	_menu->chields.push_back( new ImSeparator() );
	_menu->chields.push_back( dfRadius );
	_menu->chields.push_back( iiTime );
	_menu->chields.push_back( iiAlpha );
	_menu->chields.push_back( new ImSeparator() );
	_menu->chields.push_back( iiLimit );

	g_class.samp->input()->addChatCommand( chat__command(), this, &ColoredTracers::command );

	if ( trace__limit() != 16 ) {
		if ( trace__limit() < 16 ) trace__limit() = 16;
		reallocateTracesTable( trace__limit() );
	}

	g_class.events->onMainLoop += [this]() {
		if ( _aTraces != (CBulletTrace *)0xC7C748 )
			for ( uint i = 0; i < trace__limit(); ++i ) {
				uint sTime = _aTraces[i].createTime;
				uint time = *(uint *)0xB7CB84;
				if ( _aTraces[i].isExist && time - sTime > _aTraces[i].lifeTime ) _aTraces[i].isExist = false;
			}
	};

	SF::Instance()->Log( "{FF00FF00}Plugin %s has loaded", PROJECT_NAME.c_str() );
}

ColoredTracers::~ColoredTracers() {
	delete _hook_limit;
	memsafe::copy( (void *)0x72388F, {0x83, 0xFE, 0x10, 0x7D, 0x63, 0x8B, 0xCE} );

	auto crutch = trace__limit();
	restoreTracesTable();
	trace__limit() = crutch;

	g_class.samp->input()->deleteCommand( chat__command() );

	for ( auto &item : _menu->chields ) delete item;
	delete _menu;

	delete _hook_color;
	delete _hook_addTrace;
	delete _hook_calc;

	delete _hook_render2;
	delete _hook_render;

	SF::Instance()->Log( "{FFFF0000}Plugin %s has unloaded", PROJECT_NAME.c_str() );
}

void ColoredTracers::command( char * ) {
	g_class.cursor->toggleCursor( g_class.cursor->isLocalCursorHiden() );
	_menu->toggle( g_class.cursor->isLocalCursorShowed() );
	g_class.samp->chat()->lockChatOnCharT( g_class.cursor->isLocalCursorShowed() );
}

void ColoredTracers::updateOldTracers() {
	for ( int i = 0; i < trace__limit(); ++i ) {
		_aTraces[i].alpha = trace__alpha();
		_aTraces[i].lifeTime = trace__time();
		_aTraces[i].radius = trace__radius();
	}
}

void ColoredTracers::reallocateTracesTable( uint size ) {
	if ( _aTraces != (CBulletTrace *)0xC7C748 ) restoreTracesTable();

	if ( size < 8 ) {
		SF::Instance()->Log( "%s ~> Warning: Cannot create a table that is smaller than 8.",
							 PROJECT_NAME.data() );
		return;
	}
	if ( size % 8 != 0 ) {
		SF::Instance()->Log( "%s ~> Warning: Сannot create a table whose size is not a multiple of 8.",
							 PROJECT_NAME.data() );
		return;
	}

	_aTraces = new CBulletTrace[size];
	trace__limit() = size;
	setTracesTable();
	// Там где используется адрес 0xC7CA4C это aTraces + 0x18 (isExists) + 0x2EC (0x2C0 + 0x2C)
	// Там где используется адрес 0xC7CA10 это aTraces + 0x8 (from.fZ) + 0x2C0
}

void ColoredTracers::restoreTracesTable() {
	if ( _aTraces != (CBulletTrace *)0xC7C748 ) delete[] _aTraces;
	_aTraces = (CBulletTrace *)0xC7C748;
	trace__limit() = 16;
	setTracesTable();
}

void ColoredTracers::setTracesTable() {
	// CBulletTrace::AddTrace -> add trace to table
	memsafe::write<RwV3D *>( (void *)( 0x7238A0 ), &_aTraces->from );
	memsafe::write<RwV3D *>( (void *)( 0x7238B8 ), &_aTraces->to );
	memsafe::write<bool *>( (void *)( 0x7238E5 ), &_aTraces->isExist );
	memsafe::write<uint32_t *>( (void *)( 0x7238D1 ), &_aTraces->createTime );
	memsafe::write<uint32_t *>( (void *)( 0x7238F2 ), &_aTraces->lifeTime );
	memsafe::write<float *>( (void *)( 0x7238EC ), &_aTraces->radius );
	memsafe::write<byte *>( (void *)( 0x7238DB ), &_aTraces->alpha );
	// CBulletTrace::AddTrace -> loops
	memsafe::write<uint *>( (void *)( 0x723756 ),
							(uint *)( (uint)&_aTraces->isExist + sizeof( CBulletTrace ) ) );
	memsafe::write<uint *>( (void *)( 0x7237B0 ),
							(uint *)( (uint)&_aTraces->isExist + sizeof( CBulletTrace ) +
									  sizeof( CBulletTrace ) * trace__limit() ) );
	memsafe::write<uint *>( (void *)( 0x72380C ),
							(uint *)( (uint)&_aTraces->isExist + sizeof( CBulletTrace ) ) );
	memsafe::write<uint *>( (void *)( 0x723865 ),
							(uint *)( (uint)&_aTraces->isExist + sizeof( CBulletTrace ) +
									  sizeof( CBulletTrace ) * trace__limit() ) );
	// CBulletTraces::Render -> loop
	memsafe::write<float *>( (void *)( 0x723CB9 ), &_aTraces->from.fZ );
	memsafe::write<uint *>( (void *)( 0x723F59 ),
							(uint *)( (uint)&_aTraces->from.fZ + sizeof( CBulletTrace ) * trace__limit() ) );

	// WARN: CBulletTraces::Update не чистит кастомный пул даже с патчем. + Заменяет this в GTA-классе, что не
	// дает изменять размер пула во время игры CBulletTraces::Update -> loop
	//	memsafe::write<uint32_t *>( (void *)( 0x723FB9 ), &_aTraces->lifeTime );
	//	memsafe::write<uint *>( (void *)( 0x723FB9 ),
	//							(uint *)( (uint)&_aTraces->lifeTime + sizeof( CBulletTrace ) * trace__limit()
	//)
	//);
}

void ColoredTracers::hook_CBulletTraces__Render_start() {
	_bulletCollection = (CBulletTrace *)( *(uint *)0x7238A0 );
	_activeBullet = 0;
}

void ColoredTracers::hook_CBulletTraces__Render_drawTrace() {
	if ( !_bulletCollection[_activeBullet].isExist ) return;

	int sTime = _bulletCollection[_activeBullet].createTime;
	_hook_render2->setReg86( r86::EDI, _colors[sTime] );
	++_activeBullet;
}

void ColoredTracers::hook_CBulletTraces__AddTrace( RwV3D *&start, RwV3D *&end ) {
	// TODO: Брать эти координаты для длинных трасеров, так же сюда можно перенести поиск игрока
	_trStart = *start;
	_trEnd = *end;
	auto *pPlayerPool = g_class.samp->pPools->pPlayer;
	float dist = 99999.0f;
	_activePlayerId = -1;

	if ( LOCAL_PLAYER != nullptr ) {
		if ( !LOCAL_PLAYER->isActorDead() ) {
			_activePlayerId = pPlayerPool->sLocalPlayerID;
			dist = ( LOCAL_PLAYER->getBonePos( BodyPed::Arms::Right::Wrist ) - _trStart ).length();
		}
	}

	for ( int i = 0; i < 1004; ++i ) {
		if ( !pPlayerPool->iIsListed[i] ) continue;
		if ( pPlayerPool->pRemotePlayer[i]->pPlayerData == nullptr ) continue;
		if ( pPlayerPool->pRemotePlayer[i]->pPlayerData->pSAMP_Actor == nullptr ) continue;

		auto *pPed = pPlayerPool->pRemotePlayer[i]->pPlayerData->pSAMP_Actor->pGTA_Ped;
		if ( pPed == nullptr ) continue;
		if ( pPed->matrix == nullptr ) continue;

		RwV3D handPos = pPed->getBonePos( BodyPed::Arms::Right::Wrist ) - _trStart;
		if ( handPos.length() < dist ) {
			dist = handPos.length();
			_activePlayerId = i;
		}
	}
}

void ColoredTracers::hook_CBulletTrace__AddTrace( RwV3D *&start, RwV3D *&end, float &radius, uint &time,
												  byte &alpha ) {
	auto findCountAndEmptyAndOlder = []( CBulletTrace *traces, uint limit ) {
		int empty = -1;
		int older = 0;
		uint32_t olderTime = 0xFFFFFFFF;
		for ( int i = 0; i < limit; ++i ) {
			if ( traces[i].isExist ) {
				if ( traces[i].createTime < olderTime ) {
					olderTime = traces[i].createTime;
					older = i;
				}
			} else if ( empty == -1 )
				empty = i;
		}
		return std::make_tuple( empty, older );
	};

	auto [empty, older] = findCountAndEmptyAndOlder( _aTraces, trace__limit() );
	if ( trace__replaceOlder() && empty == -1 ) _aTraces[older].isExist = false;

	if ( trace__longTracers() ) {
		if ( !_trStart.isClear() ) start->operator=( _trStart );
		if ( !_trEnd.isClear() ) end->operator=( _trEnd );
		// Это что бы правильно рисовался трасер при прямом вызове CBulletTrace::AddTrace
		_trStart.clear();
		_trEnd.clear();
	}
	radius = trace__radius();
	time = trace__time();
	alpha = trace__alpha();
}

void ColoredTracers::hook_CBulletTrace__AddTrace_color() {
	if ( _activePlayerId == -1 ) {
		SF::Instance()->Log( "%s ~> Warning: Bullet without owner. Setted black color." );
		_colors[_hook_color->reg86( r86::EAX )] = eCdBlack;
		return;
	}

	auto *pPlayerPool = g_class.samp->pPools->pPlayer;

	_colors[_hook_color->reg86( r86::EAX )] = pPlayerPool->playerColor( _activePlayerId ).argb;
}

void ColoredTracers::hook_CBulletTrace__AddTrace_more120() {
	_hook_limit->setReg86( r86::ECX, trace__limit() );
}
